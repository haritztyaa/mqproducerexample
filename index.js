const config = require('config');
var log = require('simple-node-logger').createSimpleLogger('log.log');
log.setLevel(config.get('LogLevel'));
var stompit = require('stompit');
var dateFormat = require('dateformat');
 
var connectOptions = config.get('BrokerConnection');
//Stompit get Data
const sql = require('mssql')
 
async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}
async function getData(type) {
    try {
          const configdb = config.get("DBConnection");
          await sql.connect(configdb);
          var result = {};
          switch(type){
            case ('webPushPriceLatest'):
                result = await sql.query`select distinct	RTRIM(LTRIM(a.SecurityCode)) SecurityCode, RTRIM(LTRIM(a.board)) board, RTRIM(LTRIM(a.SecurityType)) SecurityType, a.priceLatest, a.DTCreate from
                (SELECT SS.SecurityCode, SS.BoardCode board, 
                (case when SS.ClosingPrice = 0 then SS.PreviousPrice
                else SS.ClosingPrice end) priceLatest, SD.SecurityType, SS.DTCreate
                   FROM [JSXDatastream].[dbo].[DF_StocksSummary] SS
                   left join [JSXDatastream].[dbo].[DF_StockData] SD
                   on ss.SecurityCode = sd.SecurityCode) a,
                (select SecurityCode, BoardCode, MAX(DTCreate) as DTCreate FROM [JSXDatastream].[dbo].[DF_StocksSummary]
                group by SecurityCode, BoardCode) b
                where a.SecurityCode = b.SecurityCode and a.DTCreate = b.DTCreate and a.board = b.BoardCode`
                //trim string
                await asyncForEach(result.recordset, async(res)=>{
                  //await console.log(res.SecurityCode, res.board, res.SecurityType);
                });
                break;
              case ('webPushBankNego'):
                  result = await sql.query`select distinct RTRIM(LTRIM(a.SecurityCode)) SecurityCode, RTRIM(LTRIM(a.board)) board, RTRIM(LTRIM(a.SecurityName)) SecurityName, RTRIM(LTRIM(a.SecurityType)) SecurityType, a.PreviousPrice, a.HighestPrice, a.LowestPrice, a.priceLatest, a.Change, a.TradedVolume, a.TradedValue, a.TradedFrequency, a.DTCreate from
                  (SELECT SS.SecurityCode, SD.SecurityName, SD.SecurityStatus, SS.BoardCode board, 
                  (case when SS.ClosingPrice = 0 then SS.PreviousPrice
                  else SS.ClosingPrice end) priceLatest, SS.PreviousPrice, SS.HighestPrice, SS.LowestPrice, SS.Change, SS.TradedVolume, SS.TradedValue, SS.TradedFrequency, SD.SecurityType, SD.Sector, SS.DTCreate
                     FROM [JSXDatastream].[dbo].[DF_StocksSummary] SS
                     left join [JSXDatastream].[dbo].[DF_StockData] SD
                     on ss.SecurityCode = sd.SecurityCode) a,
                  (select SecurityCode, BoardCode, MAX(DTCreate) as DTCreate FROM [JSXDatastream].[dbo].[DF_StocksSummary]
                  group by SecurityCode, BoardCode) b
                  where a.SecurityCode = b.SecurityCode and a.DTCreate = b.DTCreate and a.board = b.BoardCode and a.Sector = 81 and a.board = 'NG'`
   
                  //trim string
                  await asyncForEach(result.recordset, async(res)=>{
                    //await console.log(res.SecurityCode, res.board, res.SecurityType);
                  });
                  break;
          }
          return {data: result.recordset};
        
    } catch (err) {
      console.log(err);
      log.error('select database failed at: ', new Date().toJSON());
    }
}

const run = async()=>{
  const validMethod = ['webPushPriceLatest', 'webPushBankNego']
  // validate arguments
  if(validMethod.indexOf(process.argv[2])<=-1){
    console.log('parameter yang dimasukkan salah, cara penggunaan yang benar:');
    console.log('node '+process.argv[1]+' [method valid: '+validMethod+'] ');
    console.log();
    process.exit();
  }

  //process 
  var result = JSON.stringify(await getData(process.argv[2]), null, 4);
  sendData(result, process.argv[2]);
  // console.log(result);
}
run();
// module.exports = run;














//Stompit send
async function sendData(data, method){
  var reconnectOptions = {
    'maxReconnects': 10 
  };
  var manager = new stompit.ConnectFailover(connectOptions, reconnectOptions);
  manager.connect(function(error, client, reconnect) {
  
    if (error) {
      console.log('connect error ' + error.message);
      log.warn('failed to connect: ', error.message, ' failed at ', new Date().toJSON());
      return;
    };
    client.on('error', function(error) {

      // calling reconnect is optional and you may not want to reconnect if the
      // same error will be repeated.
      
      log.warn('failed to connect: ', error, ' failed at ', new Date().toJSON());
      reconnect();
    });
    var id = ''+method+dateFormat(new Date(), 'yyyymmddHHMM');
    var sendHeaders = {
      'destination': 'hello',
      'content-type': 'text/plain',
      'correlation-id': id,
      'expires': '0',
      'JMSXGroupID':id,
      'persistent': true,
      'idxNumber': 1,
      'idxTotal': 1,
      'idxCmdType' : 'push',
      'idxFiletype' : 'form',
      'idxFlowType' : 'req',
      'idxMethod' : method,
      'idxSender' : 'web',
      'note': 'out'
    };
    var datasend = {header: sendHeaders, ...JSON.parse(data)}
    var frame = client.send(sendHeaders);
    try{
      frame.write(JSON.stringify(datasend, null, 4));
      frame.end();
      log.info('send to ', sendHeaders, ' success at ', new Date().toJSON());
      client.disconnect();
    }catch(error){
      log.warn('send to SOA error: ', error, ' failed at ', new Date().toJSON());
    }
    
  });
}